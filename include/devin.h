#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <MLV/MLV_all.h>

#define TAILLE_MAX 1024
#define POPULATIONS_FILENAME 	"../data/populations.txt"
#define QUESTIONS_FILENAME 	"../data/questions.txt"
int NB_QUESTIONS; 
int NB_POPULATION;

typedef struct question{
	int numQuestion, valeur;
	struct question *suivant;
} Question, *ListeQuestions, *ListeReponses;

typedef struct personnage{
	char* nameCharacter; 
	int noteCharacter;
	int lineCharacter;
	struct personnage *suivant;
} Personnage, *ListePersonnages;

/* fonctions.c */
char* readLineFromFile(char* fileName, int line);
void triQuestions( ListeQuestions *lst );
ListePersonnages triPersonnages( ListePersonnages lst );
void initPersonnagesAvecNote( ListePersonnages *lst );
ListeReponses askQuestions( ListeQuestions lst, ListePersonnages *lstP );

void changerVariable( int *variable, int val );
int getNumberOfLines( FILE *fp );
void afficherTexteFenetre(char* texte, MLV_Image* fond);
char* addAntiSlashN( char* texte ) ;
/* listeChainee.c */
int insererEnTete( ListeQuestions *lst, int numQuestion, int reponse );
int insererEnTeteChar( ListePersonnages *lst, char *name, int valeur, int line );
int insererAPlace( ListeQuestions* lst, int numQuestion, int valeur );
int insererAPlaceChar( ListePersonnages* lst, char* name, int valeur,int lineCharacter );
int lireListe( ListeQuestions *lst, int numQuestion );
void afficherListe(ListeQuestions lst);
void afficherListeChar(ListePersonnages lst);
int removeBadCharacter( ListePersonnages *liste );
int nbElementListe(ListePersonnages lst);
/* finPartie.c */
void updateFromUserResponses( int line, ListeReponses repUser );
char getUserResponse( ListeReponses repUser, int numQuestion );
void programFailed( ListeReponses lst, char* personnage, char* question );
/* partieGraphique.c */
void home();
void help( MLV_Image* fond, MLV_Image* parchment, MLV_Image* retour );
void play(MLV_Image* fondPartie );
char* boiteDeSaisie( MLV_Image* fond, char* message );
void afficherTexteFenetre( char* texte, MLV_Image* fond );
void genieDevinePersonnage ( ListePersonnages listP, MLV_Image* fondPartie, ListePersonnages *meilleursPersonnages );
void guessedRightCharacter ( ListePersonnages meilleursPersonnages, ListeReponses repUser );
void guessedWrongCharacter ( ListePersonnages meilleursPersonnages, MLV_Image *fondPartie, ListeReponses repUser );

void afficherTexte();
void printArray(int* tab, int taille);
void initTab(int* tab, int taille);
