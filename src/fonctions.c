#include "../include/devin.h"


int getNumberOfLines( FILE *fp ) { 
	int n = 0, c; 
	while ( (c = fgetc(fp) ) != EOF ) { 
		if ( c == '\n' )
			n++;;
	} 
	return n;
}

void changerVariable( int *variable, int val ) {
	*variable = val; 
}

Personnage* findBestCharacter(ListePersonnages lstP) {
/* Trouve le personnage le plus adapté en fonction des réponses du joueur */
    int meilleurNote = -70;
    Personnage *p = NULL;
	while ( lstP != NULL ) {
        if ( lstP->noteCharacter > meilleurNote ) {
            meilleurNote = lstP->noteCharacter;
            p = lstP;
        }
    }
    return p;
}

int calculNoteForQuestionFromListe( ListePersonnages lst, int numeroQuestion ) {
/* Calcule & retourne une note pour chaque question (permettra de trouver la question la + pertinente) mais calcule cette fois à partir de la liste de personnages mis à jour */
	char *tmp;
	int  reponseAttendue, tab[6];
	initTab(tab, 6);
	while (lst != NULL) {
		tmp = readLineFromFile( POPULATIONS_FILENAME, (lst)->lineCharacter+1 );	/* On récupère chaque ligne de réponses attendues */
		reponseAttendue = (int) tmp[numeroQuestion*2-2] - 48;	/* On récupère la colonne correspondant à la question qui nous intéresse */
		tab[reponseAttendue] += 1 ;		/* On incrémente la bonne case du tableau pour chaque valeur attendue */
		lst = (lst)->suivant;
	}
	/* On calcule la note pour la question designée par l'argument "numeroQuestion" */
	return (tab[1]+1)*(tab[2]+1)*(tab[3]+1)*(tab[4]+1)*(tab[5]+1);
}

void UpdateTriQuestions( ListeQuestions lst, ListePersonnages lst2, ListeQuestions* new ) {
	*new = NULL;
	while ( lst != NULL ) {
		insererAPlace( new, (lst)->numQuestion , calculNoteForQuestionFromListe(lst2, (lst)->numQuestion) );
		lst = lst->suivant;
	}
}

void updateCharacterNote(ListePersonnages *lst, int line, int note){
	ListePersonnages tmp = *lst;
	while ( tmp != NULL && line != tmp->lineCharacter )
		tmp = tmp->suivant;
	tmp->noteCharacter += note;
	removeBadCharacter(lst);
}

void compareSaisieAttendue(ListePersonnages *lst, int line, int reponseAttendue, int reponseSaisie) {
	int note = 0, result;
	if ( reponseAttendue != 0) {
		result = abs( reponseAttendue - reponseSaisie );
		switch ( result ) {
			case 0: note += 3; break;
			case 1: note += 1; break;
			case 2: note -= 1; break;
			case 3: note -= 2; break;
			case 4: note -= 3; break;
		}
		updateCharacterNote(lst, line, note);
	}
}
char* addAntiSlashN( char* texte ) {
/* Reçoit un texte d'une seule ligne en paramètre puis retourne ce texte sur plusieurs lignes */
    int tailleMot, tailleNouveauMot, i, condition = 0, compteur = 0;
    tailleMot = strlen(texte);
    tailleNouveauMot = tailleMot + tailleMot / 30;
    char *nouveauMot = (char*) malloc ( sizeof(char)*(tailleNouveauMot+1));
    
    for( i = 0; i < tailleNouveauMot; i++ ) {
        if ( i != 0 && i % 30 == 0 )
            condition = 1;
        if ( condition == 1 && texte[i - compteur] == ' ' ) {
            nouveauMot[i] = '\n';
            compteur++;
            condition = 0;
        }
        else
            nouveauMot[i] = texte[i - compteur];
    }
    nouveauMot[tailleNouveauMot] = '\0';
    return nouveauMot;
}

ListeReponses askQuestions( ListeQuestions lst, ListePersonnages *lstP ) {
/* On parcourt la liste des questions & les pose selon l'ordre de pertinence */
	MLV_Image* fondPartie = MLV_load_image("../data/fondPartie.png");
	ListeReponses rep = NULL;
	ListePersonnages temp = NULL,temp2 = NULL;
	ListeQuestions new = lst;
	int nbQuestionsPosees = 0;
	char *question = (char*) malloc(sizeof(char) * TAILLE_MAX );
	char *tmp;
	int saisie, reponseAttendue;

	while ( new != NULL && nbQuestionsPosees < 20) {
		UpdateTriQuestions(lst,*lstP, &new);
		question = readLineFromFile( QUESTIONS_FILENAME, new->numQuestion );
		afficherTexteFenetre(addAntiSlashN(question), fondPartie);
		nbQuestionsPosees++;
		int x, y, boutonClique = 1;
		do {
			MLV_wait_mouse(&x,&y);
			if ( x >= 80 && x < 270 && y >= 665 && y <= 730 ) { /*Si on clique sur surement*/
				saisie = 1;
				boutonClique =0;
			}
			else if ( x >= 190 && x < 380 && y >= 575 && y <= 640 ) {
				saisie = 2;
				boutonClique =0;
			}
			else if ( x >= 300 && x < 490 && y >= 665 && y <= 730 ) {
				saisie = 3;
				boutonClique =0;
			}
			else if ( x >= 420 && x < 610 && y >= 575 && y <= 640 ) {
				saisie = 4;
				boutonClique =0;
			}
			else if ( x >= 520 && x < 710 && y >= 665 && y <= 730 ) {
				saisie = 5;
				boutonClique =0;
			}
		}while (boutonClique);
		/* On remplit la liste des réponses données par l'utilisateur avec sa nouvelle saisie */
		insererEnTete( &rep, new->numQuestion, saisie);
		temp = *lstP, temp2 = *lstP;

		while ( temp != NULL ) {
			tmp = readLineFromFile( POPULATIONS_FILENAME, (temp->lineCharacter)+1 );
			reponseAttendue = (int) tmp[(new)->numQuestion*2-2] - 48;
			compareSaisieAttendue(lstP, temp->lineCharacter, reponseAttendue, saisie);
			temp = temp->suivant;
		}
		lstP = &temp2;
		new = new->suivant;
		lst = new;	
		free(question);
	}
	MLV_free_image(fondPartie);
	return rep;
}

char* readLineFromFile(char* fileName, int line) {
/* Retourne la ligne "line" du fichier dont le nom est donné en paramètre via "fileName" */
	FILE* file = fopen(fileName, "r");
	int compteur = 0;
	char *ligne = malloc(1024 * sizeof(char));
	if ( file == NULL )
		return NULL;
	while ( fgets(ligne, 1024, file) ) {
		if ( compteur == line-1 )
			break;
		compteur++;
	}
	fclose(file);
	return ligne;
}

void initPersonnagesAvecNote( ListePersonnages *lst ) {
/* Crée une liste de tous les personnages en initialisant leur note à 0 */
	int line;
	char* personnage;
	for( line = 1; line < NB_POPULATION*2; line += 2 ) {
		personnage = readLineFromFile(POPULATIONS_FILENAME, line);
		personnage[strlen(personnage)]='\0';
		insererEnTeteChar(lst, personnage, 0, line); 
	}
}

int calculNoteForQuestion( int numeroQuestion ) {
/* Calcule & retourne une note pour chaque question (permettra de trouver la question la + pertinente) */
	char *tmp;
	int i, reponseAttendue, tab[6];
	initTab(tab, 6);
	for ( i = 2; i <= NB_POPULATION*2; i += 2 ) {
		tmp = readLineFromFile( POPULATIONS_FILENAME, i );	/* On récupère chaque ligne de réponses attendues */
		reponseAttendue = (int) tmp[numeroQuestion*2-2] - 48;	/* On récupère la colonne correspondant à la question qui nous intéresse */
		tab[reponseAttendue] += 1 ;		/* On incrémente la bonne case du tableau pour chaque valeur attendue */
	}
	/* On calcule la note pour la question designée par l'argument "numeroQuestion" */
	return (tab[1]+1)*(tab[2]+1)*(tab[3]+1)*(tab[4]+1)*(tab[5]+1);
}

void triQuestions( ListeQuestions *lst ) {
/* Crée une liste de questions triée selon leur pertinence (de la + à la - pertinente) */
	int numeroQuestion;
	for( numeroQuestion = 1; numeroQuestion <= NB_QUESTIONS; numeroQuestion++ )
		insererAPlace(lst, numeroQuestion, calculNoteForQuestion( numeroQuestion));
}

ListePersonnages triPersonnages( ListePersonnages lst ) {
/* Crée une liste de questions triée selon leur pertinence (de la + à la - pertinente) */
	ListePersonnages tmp = NULL;
	while(lst != NULL){
		insererAPlaceChar(&tmp, lst->nameCharacter, lst->noteCharacter, lst->lineCharacter );
		lst = lst->suivant;
	}
	return tmp;
}

void printArray(int* tab, int taille) {
	int i;
	for( i = 0; i < taille; i++)
		printf("%d ", tab[i]);
}

void initTab(int* tab, int taille) {
	int i;
	for( i = 0; i < taille; i++ )
		tab[i] = 0;
}

