#include "../include/devin.h"

int nbElementListe(ListePersonnages lst){
	/*retourne le nombre d'élémnents d'une liste de personnages*/
	int compt = 0;
	if (lst == NULL)
		return 0;
	while (lst != NULL){
		compt++;
		lst = lst->suivant;
	}
	return compt;
}

Question *allouerCellule(int numQuestion, int reponse) {
/* Alloue de l'espace mémoire pour une cellule */
	Question* nouv =(Question *)malloc(sizeof(Question));
	if(nouv) {
		nouv->numQuestion = numQuestion;
		nouv->valeur = reponse;
		nouv->suivant = NULL;
	}
	else
		return NULL;
	return nouv;
}

Personnage *allouerCelluleChar( char* name, int valeur, int line ) {
/* Alloue de l'espace mémoire pour une cellule */
	Personnage* nouv =(Personnage *)malloc(sizeof(Personnage));
	if(nouv) {
		nouv->nameCharacter = name;
		nouv->noteCharacter = valeur;
		nouv->lineCharacter = line;
		nouv->suivant = NULL;
	}
	else
		return NULL;
	return nouv;
}

int insererEnTete( ListeQuestions *lst, int numQuestion, int reponse ) {
/* Insère dans une liste chaînée les réponses données par l'utilisateur */
	ListeQuestions temp = allouerCellule(numQuestion, reponse);
	if( temp == NULL )
		return 0;
	temp->suivant = *lst;
	*lst = temp;
	return 1;
}

int insererEnTeteChar( ListePersonnages *lst, char *name, int valeur, int line ) {
	ListePersonnages temp = allouerCelluleChar(name, valeur, line);
	if( temp == NULL )
		return 0;
	temp->suivant = *lst;
	*lst = temp;
	return 1;
}

int insererAPlace( ListeQuestions* lst, int numQuestion, int valeur ) {
/* Insère dans une liste chaînée des questions selon leur note. La question la + pertinente se trouve en tête */
	ListeQuestions tmp = *lst;
	Question* cel = allouerCellule(numQuestion, valeur);
	if( tmp == NULL ) {		
		tmp = cel;
		*lst = tmp;
		return 0;	
	}
	if( tmp != NULL && valeur > tmp->valeur ) {	
		cel->suivant = tmp;
		tmp = cel;
		*lst = tmp;
		return 0;		
	}
	while( tmp->suivant != NULL && valeur < tmp->suivant->valeur )
		tmp = tmp->suivant;
	cel->suivant = tmp->suivant;
	tmp->suivant = cel;
	return 0;		
}

int insererAPlaceChar( ListePersonnages* lst, char* name, int valeur,int lineCharacter ) {
/* Insère dans une liste chaînée des questions selon leur note. La question la + pertinente se trouve en tête */
	ListePersonnages tmp = *lst;
	Personnage* cel = allouerCelluleChar(name, valeur, lineCharacter);
	if( tmp == NULL ) {		
		tmp = cel;
		*lst = tmp;
		return 0;	
	}
	if( tmp != NULL && valeur > tmp->noteCharacter ) {	
		cel->suivant = tmp;
		tmp = cel;
		*lst = tmp;
		return 0;		
	}
	while( tmp->suivant != NULL && valeur < tmp->suivant->noteCharacter )
		tmp = tmp->suivant;
	cel->suivant = tmp->suivant;
	tmp->suivant = cel;
	return 0;		
}

int removeBadCharacter( ListePersonnages *liste ) {
/* Supprime de la liste des personnages celui ayant une note inférieure ou égale à 10 */
	if( *liste == NULL )
		return 0;
	while ( (*liste) != NULL ) {
		if ( (*liste)->noteCharacter <= -10 )
			(*liste) = (*liste)->suivant;
		else
			(liste) = &( (*liste)->suivant );
 	}
  	return 1;
}

int lireListe( ListeQuestions *lst, int numQuestion ) {
/* Récupère la saisie de l'utilisateur & appelle la fonction insererEnTete pour constituer la liste */
	int saisie;
	afficherTexte();
	printf("Reponse : ");
	scanf("%d", &saisie);
	insererEnTete( lst, numQuestion, saisie );
	return 1;
}

void afficherListeChar(ListePersonnages lst) {
/* Affiche une liste */
	while( lst != NULL ) {
		printf("Personnage %s Note = %d Ligne : %d \n", lst->nameCharacter, lst->noteCharacter, lst->lineCharacter);
		lst = lst->suivant;
	}
	printf("\n");
}

void afficherListe(ListeQuestions lst) {
/* Affiche une liste */
	while( lst != NULL ) {
		printf("Question n° %d , Valeur = %d \n", lst->numQuestion, lst->valeur);
		lst = lst->suivant;
	}
	printf("\n");
}

void afficherTexte() {
	printf("0) non connaissance de la réponse\n1) surement\n2) probablement oui\n3) ne sais pas\n4) probablement non\n5) surement pas\n");
}

