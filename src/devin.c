#include "../include/devin.h"

int main(int argc, char *argv[]) {
	/* Ces 4 lignes nous permettent de mettre à jour le nombre de questions & de personnages pour la suite du programme */
	FILE* pop = fopen(POPULATIONS_FILENAME, "r");
	FILE* question = fopen(QUESTIONS_FILENAME, "r");
	changerVariable(&NB_QUESTIONS, getNumberOfLines(question));
	changerVariable(&NB_POPULATION, getNumberOfLines(pop)/2);;
	fclose( pop );
	fclose( question );

	MLV_create_window("Akinator","Akinator",800,750);
	home(); 
	MLV_free_window();
	return 0;
}

