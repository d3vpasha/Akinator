#include "../include/devin.h"

/* 
	Ce fichier contient les fonctions qui seront exécutés lorsque la partie se termine :
	- soit le programme trouve juste & les données du personnage concerné sont mis à jour grâce à la fonction "updateFromUserResponses",
	- soit le programme se trompe, alors on prévoit un mécanisme pour rajouter le personnage si ce dernier n'existait pas dans les données du programme & on propose à 		l'utilisateur de rajouter une question qui aurait permis d'identifier son personnage,
*/

/* SI LE PROGRAMME TROUVE JUSTE */

char getUserResponse( ListeReponses repUser, int numQuestion ) {
/* Renvoie la réponse de l'utilisateur pour une question dont le numéro est donné en paramètre */
	while ( repUser != NULL ) {
	/* On parcourt la liste des réponses de l'utilisateur & lorsqu'on tombe sur la question concernée, on retourne la réponse de l'utilisateur */
		if ( repUser->numQuestion == numQuestion )
			return repUser->valeur + 48;
		repUser = repUser->suivant;
	}
	return '0';
}

void updateFromUserResponses( int line, ListeReponses repUser ) {
/* Si le programme trouve juste, les données du personnage concerné sont mis à jour grâce aux deux fonctions ci-dessous */
	FILE *fichier = fopen(POPULATIONS_FILENAME, "r+");
	int compteur = 0, i = 1, lastValue;
	char* lineContent;
	/* Ce while nous permettra d'aller à la ligne du personnage dont on veut mettre les infos à jour */
	while ( i <= line ) {
		lineContent = readLineFromFile ( POPULATIONS_FILENAME, i );
		compteur += strlen ( lineContent );
		i++;
	}
	/* Pour arrêter les modifications juste avant -1 */
	lineContent = readLineFromFile ( POPULATIONS_FILENAME, line+1 );
	lastValue = strlen ( lineContent ) - 3 + compteur;
	/* Une fois arrivé à la bonne ligne, on remplace les 0 par les réponses données par l'utilisateur */
	for ( i = compteur; i < lastValue; i += 2 ) {
		/* On lit le caractère & s'il vaut 0, on remplace par la réponse de l'utilisateur */
		fseek( fichier, i, SEEK_SET );
		char c = fgetc( fichier );
		if( c == '0' || c == '3') {
			fseek( fichier, i, SEEK_SET );
			fputc( getUserResponse( repUser, (i-compteur)/2+1 ), fichier );
		}
	}
	fclose( fichier );
}

/* SI LE PROGRAMME SE TROMPE */

void addNewRowInPopulationsFile() {
/* Dans le fichier des personnages (populations.txt), à la ligne des réponses attendues, on rajoute une colonne pour la nouvelle question */
	FILE* fichier = fopen( POPULATIONS_FILENAME, "r" );
	FILE* tmp = fopen( "../data/tmp.txt", "w" );
	char charLu;
	/* On écrit le contenu de populations.txt dans tmp.txt en rajoutant en + la nouvelle colonne correspondant à la nouvelle question */
	do {
		charLu = fgetc( fichier );
		if ( charLu != EOF ) {
			if ( charLu == '-' )	fputs ( "0 -", tmp );
			else					fputc ( charLu, tmp );
		}
	} while ( charLu != EOF );
	fclose(fichier);
	fclose(tmp);
	/* Puis on copie le contenu de tmp.txt dans populations.txt */
	fichier = fopen( POPULATIONS_FILENAME, "w" );
	tmp = fopen( "../data/tmp.txt", "r" );
	do {
		charLu = fgetc( tmp );
		if ( charLu != EOF )
			fputc ( charLu, fichier );
	} while ( charLu != EOF );
	fclose(fichier);
	fclose(tmp);
}

int addQuestion( ListeReponses lst, char* question ) {
/* Ajoute une question qui aurait permis d'identifier le personnage & retourne la réponse qu'aurait donné l'utilisateur à cette question pour le personnage */
    MLV_Image *fondPartie = MLV_load_image("../data/fondPartie.png");
	int saisie, x, y, boutonClique = 1;
    FILE* fichier = fopen( QUESTIONS_FILENAME, "at" );
    /* A la fin du fichier des questions (questions.txt), on rajoute la question qui aurait permis d'identifier le personnage */
    fputs( question, fichier);
    /* Obtient la réponse qu'aurait donné l'utilisateur à la question pour le personnage concerné */
    afficherTexteFenetre(addAntiSlashN("Veuilez cliquer sur la réponse à cette question pour ce personnage."), fondPartie);
    do {
			MLV_wait_mouse(&x,&y);

			if ( x >= 80 && x < 270 && y >= 665 && y <= 730 ) { /*Si on clique sur surement*/
				saisie = 1;
				boutonClique =0;
			}

			else if ( x >= 190 && x < 380 && y >= 575 && y <= 640 ) {
				saisie = 2;
				boutonClique =0;
			}

			else if ( x >= 300 && x < 490 && y >= 665 && y <= 730 ) {
				saisie = 3;
				boutonClique =0;
			}

			else if ( x >= 420 && x < 610 && y >= 575 && y <= 640 ) {
				saisie = 4;
				boutonClique =0;
			}

			else if ( x >= 520 && x < 710 && y >= 665 && y <= 730 ) {
				saisie = 5;
				boutonClique =0;
			}
	}while (boutonClique);
    /* Dans le fichier des personnages (populations.txt), à la ligne des réponses attendues, on rajoute une colonne pour la nouvelle question */
    addNewRowInPopulationsFile();
    fclose( fichier );
    NB_QUESTIONS++;
    return saisie;
}


int characterExists(char* name){
	/* 	Renvoie 0 si le personnage n'existe pas dans les données du programme
	 	Sinon, renvoie la ligne où se trouve le personnage	*/
	int i,j, cmpt = 0;
	char* personnage = (char*)malloc(sizeof(char)*TAILLE_MAX);
	for(i = 1; i <= NB_POPULATION*2; i = i+2){

		personnage = readLineFromFile(POPULATIONS_FILENAME,i);
		for(j = 0; j < strlen(name)-2; j++) {
			if (name[j] == personnage[j])
				cmpt++;
		}
		if (cmpt == strlen(name)-2)
			return i;
		else 
			cmpt = 0;
	}

	free(personnage);
	return 0;
}

void addCharacter ( ListeReponses lst, char* personnage ) {
/* Un nouveau personnage est ajouté dans le fichier populations.txt ainsi que les réponses attendues correspondantes */
	FILE* fichier = fopen( POPULATIONS_FILENAME, "at" );
	if ( fichier == NULL ) {
		fprintf(stderr, "Erreur lors de l'ouverture du fichier."); 
		return; 
	}
	/* On rajoute le nom du personnage dans le fichier des personnages */
	fputs( personnage, fichier );
	/* On rajoute les réponses attendues pour ce personnage */
	int numQuestion ;
	for ( numQuestion = 1; numQuestion <= NB_QUESTIONS; numQuestion++ ) {
		fputc( getUserResponse( lst, numQuestion ), fichier );
		fputc(' ', fichier );
	}
	fclose(fichier);
	NB_POPULATION++;

}

void updateSimpleValue( int line, ListeReponses repUser, int saisie ) {
/* Pour la nouvelle question ajoutée, la réponse correspondante est mise à jour à partir de la saisie de l'utilisateur */
	FILE *fichier = fopen(POPULATIONS_FILENAME, "r+");
	int compteur = 0, i = 1;
	char* lineContent;
	/* Ce while nous permettra d'aller à la ligne du personnage dont on veut mettre les infos à jour */
	while ( i <= line ) {
		lineContent = readLineFromFile ( POPULATIONS_FILENAME, i );
		compteur += strlen ( lineContent );
		i++;
	}
	lineContent = readLineFromFile ( POPULATIONS_FILENAME, line+1 );
	compteur = strlen ( lineContent ) - 5 + compteur;
	fseek( fichier, compteur, SEEK_SET );
	fprintf( fichier, "%d", saisie );
	fclose( fichier );
}

void programFailed( ListeReponses lst, char* personnageClique, char* questionPose ) {
/* Si le programme se trompe, on rajoute un personnage (s'il n'existe pas déjà) & une question qui aurait permis d'identifier celui-ci */
	int saisie, x ,y ,boutonClique = 1;
	MLV_Image* fondPerd = MLV_load_image("../data/fondPerd.png");
	MLV_resize_image(fondPerd,800,750);

	char* personnage = personnageClique;
	/*personnage[strlen(personnage)-2] = '\n';	*/
	/* On demande à l'utilisateur le personnage auquel il a pensé pour vérifier s'il existe déjà dans les données du programme */
	/* Si le personnage n'existe pas dans les données du programme */
	if ( !characterExists(personnage) ) {
		/* On rajoute un nouveau personnage dans le fichier populations.txt */
		addCharacter( lst, personnage );
		/* 	On demande à l'utilisateur de rajouter une question qui aurait permis d'identifier son personnage & récupère sa réponse à celle-ci 
		Puis on enregistre cette réponse dans les réponses attendus correspondant au personnage en question */
		saisie = addQuestion( lst, questionPose );
		FILE* fichier = fopen( POPULATIONS_FILENAME, "at" );
		fseek( fichier, saisie, SEEK_END );
		fprintf(fichier, "%d -1\n", saisie);
		fclose(fichier);
	}
	/* Si le personnage existe dans les données du programme */
	else {
		updateFromUserResponses( characterExists(personnage), lst );
		/*	Dans le fichier questions.txt, on rajoute la nouvelle question 
			Dans le fichier populations.txt, on rajoute une nouvelle colonne pour cette nouvelle question	*/
		saisie = addQuestion( lst, questionPose );
		/* Dans le fichier populations.txt, on met à jour la réponse correspondante au personnage à la question qui vient d'être ajoutée */
		updateSimpleValue ( characterExists(personnage), lst, saisie );
	}
	MLV_clear_window(MLV_COLOR_WHITE);
	MLV_draw_image(fondPerd,0,0);
	MLV_actualise_window();
	do {   
		MLV_wait_mouse(&x,&y);
		if (x >= 529 && x <= 776 && y >= 293 && y <= 385 ) {
			home();
			boutonClique = 0;
		}
	} while (boutonClique);

	free(personnage);
}
