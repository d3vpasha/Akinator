#include "../include/devin.h"

/* Ce fichier contient toutes les fonctions qui seront appellées pour représenter graphiquement le jeu  */

void guessedWrongCharacter ( ListePersonnages meilleursPersonnages, MLV_Image *fondPartie, ListeReponses repUser ) {
/* Cette fonction sera appelée lorsque le joueur indiquera que le personnage deviné par le génie n'est pas celui auquel il a pensé */
	MLV_Image *aucun = MLV_load_image("../data/aucun.png");
	char* tabMeilleurs[5];
	char* personnageClique = NULL;
	char* questionPose = NULL;
	int x, y, buttonClique = 1, i = 0, nbElement = nbElementListe(meilleursPersonnages);

	MLV_clear_window( MLV_COLOR_WHITE );
	MLV_draw_image( fondPartie, 0, 0 );
	afficherTexteFenetre(addAntiSlashN( "Est-ce que le nom de ton personnage figure dans cette liste ? Si oui, clique sur son nom, sinon clique sur AUCUN"), fondPartie );
	nbElement = nbElement < 5 ? nbElement : 5;	/* Si le nombre de personnages restants est > à 5, on affiche les 5 meilleurs. Sinon on affiche ceux restants ( < à 5 ) */

	/* Le génie affiche le bouton AUCUN & ses autres meilleurs propositions qui sont susceptibles de correspondre au personnage deviné par le joueur */
	while(i < nbElement){
		MLV_draw_adapted_text_box(
			450, 250 + i*40, meilleursPersonnages->nameCharacter, 9,
			MLV_COLOR_BLACK, MLV_COLOR_BLUE, MLV_COLOR_GREY80, MLV_TEXT_CENTER
		);
		MLV_draw_image( aucun, 450, 470 );	/* Affiche le bouton AUCUN */
		MLV_actualise_window();
		tabMeilleurs[i] = meilleursPersonnages->nameCharacter;
		meilleursPersonnages = meilleursPersonnages->suivant;
		i++;
	}
	/* On attent que le joueur clique sur un des personnages listés ou sur le bouton AUCUN */
	do {
		MLV_wait_mouse(&x, &y);
		/* Cliquer sur un des personnages listés */
		if (x >= 450 && x <= 550 && y >= 250 && y <= 280 ) {
			personnageClique = tabMeilleurs[0];
			afficherTexteFenetre("Veuilez écrire une nouvelle question\nrelative à ce personnage.", fondPartie);
			questionPose = boiteDeSaisie(fondPartie, "Question : ");
			buttonClique=0;
		}
		else if (x >= 450 && x <= 550 && y >= 290 && y <= 320 ) {
			personnageClique = tabMeilleurs[1];
			afficherTexteFenetre("Veuilez écrire une nouvelle question\nrelative à ce personnage.", fondPartie);
			questionPose = boiteDeSaisie(fondPartie, "Question : ");
			buttonClique = 0;
		}
		else if (x >= 450 && x <= 550 && y >= 330 && y <= 360 ) {
			personnageClique = tabMeilleurs[2];
			afficherTexteFenetre("Veuilez écrire une nouvelle question\nrelative à ce personnage.", fondPartie);
			questionPose = boiteDeSaisie(fondPartie, "Question : ");
			buttonClique = 0;
		}
		else if (x >= 450 && x <= 550 && y >= 370 && y <= 400 ) {
			personnageClique = tabMeilleurs[3];
			afficherTexteFenetre("Veuilez écrire une nouvelle question\nrelative à ce personnage.", fondPartie);
			questionPose = boiteDeSaisie(fondPartie, "Question : ");
			buttonClique = 0;
		}
		else if (x >= 450 && x <= 550 && y >= 410 && y <= 440 ) {
			personnageClique = tabMeilleurs[4];
			afficherTexteFenetre("Veuilez écrire une nouvelle question\nrelative à ce personnage.", fondPartie);
			questionPose = boiteDeSaisie(fondPartie, "Question : ");
			buttonClique = 0;
		}
		/* Cliquer sur le bouton AUCUN */
		else if ( ( x >= 450 && x <= 576 && y >= 470 && y <= 544 ) || meilleursPersonnages == NULL) {
			afficherTexteFenetre("Veuillez saisir le nom\nde votre personnage", fondPartie);
			personnageClique = boiteDeSaisie(fondPartie, "Personnage : ");     
			afficherTexteFenetre("Veuilez écrire une question\nrelative à ce personnage.", fondPartie);
			questionPose = boiteDeSaisie(fondPartie, "Question : ");
			buttonClique = 0;
		}
	} while ( buttonClique );
	programFailed( repUser, personnageClique, questionPose );

	MLV_free_image(aucun);
}

void guessedRightCharacter ( ListePersonnages meilleursPersonnages, ListeReponses repUser ) {
/* Cette fonction sera appelée lorsque le joueur indiquera que le personnage deviné par le génie est celui auquel il a pensé */
	MLV_Image *fondGagne = MLV_load_image("../data/fondGagne.png");
	int x, y;	

	updateFromUserResponses( meilleursPersonnages->lineCharacter, repUser );
	MLV_resize_image(fondGagne,800,800);
	MLV_draw_image(fondGagne,0,0);
	MLV_actualise_window();
	int btnClk = 1;
	do {   
	MLV_wait_mouse(&x,&y);
	if (x >= 250 && x <= 564 && y >= 537 && y <= 647 )
		home();
	} while (btnClk);

	MLV_free_image(fondGagne);
}

void afficherTexteFenetre( char* texte, MLV_Image* fond ) {
/* Permet d'afficher un texte à l'intérieur de la bulle */
    MLV_clear_window( MLV_COLOR_WHITE );
    MLV_resize_image_with_proportions( fond, 800, 750 );
    MLV_draw_image( fond, 0, 0 );
    MLV_draw_adapted_text_box( 
        430, 100, texte, 9,
        MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_COLOR_WHITE, MLV_TEXT_CENTER
    );
    MLV_actualise_window();
}

void play( MLV_Image* fondPartie ) {
	int buttonClique = 1, x, y;
	ListePersonnages meilleursPersonnages = NULL;
	ListeReponses repUser = NULL;
	ListeQuestions listQuestions = NULL;
	ListePersonnages listPersonnages = NULL;
	MLV_Image *oui = MLV_load_image("../data/oui.png");
	MLV_Image *non = MLV_load_image("../data/non.png");

	MLV_resize_image_with_proportions( fondPartie, 800, 750 );
	MLV_draw_image( fondPartie, 0, 0 );
	MLV_actualise_window();
	/* On trie les questions selon leur pertinence */
	triQuestions( &listQuestions );
	/* On initialise les personnages avec la note 0 */
	initPersonnagesAvecNote( &listPersonnages );
	/* DEBUT DE LA PARTIE */
	/* 1 - On demande à l'utilisateur de penser très fort un personnage */
	afficherTexteFenetre( "Veuillez penser très fort à un personnage,\nje vais essayer de le deviner.", fondPartie );
	MLV_wait_seconds(3);
	/* 2 - On pose une série de questions à l'utilisateur & récupère ses réponses */
	repUser = askQuestions( listQuestions, &listPersonnages );
	/* 3 - On donne à l'utilisateur la meilleure proposition correspondant à ses réponses */
	genieDevinePersonnage ( listPersonnages, fondPartie, &meilleursPersonnages );
	/* 4 - On demande à l'utilisateur de nous informer si le personnage proposé est le bon */
	afficherTexteFenetre( "Est-ce le bon personnage ?", fondPartie );
	MLV_draw_image( oui, 300, 400 );
	MLV_draw_image( non, 490, 400 );
	MLV_actualise_window();
	do {
        MLV_wait_mouse(&x, &y);
		/* Lorsque le joueur clique sur OUI pour informer que le personnage deviné est celui auquel il a pensé  */
		if (x >= 300 && x <= 470 && y >= 400 && y <= 490) {
			guessedRightCharacter( meilleursPersonnages, repUser );
			buttonClique = 0;
		}
		/* Lorsque le joueur clique sur NON pour informer que le personnage deviné n'est pas celui auquel il a pensé  */
		else if (x >= 490 && x <= 660 && y >= 400 && y <= 490) {
			meilleursPersonnages = meilleursPersonnages->suivant;
			guessedWrongCharacter( meilleursPersonnages, fondPartie, repUser ); 
			buttonClique = 0;
		}
	} while (buttonClique);

	MLV_free_image(oui);
	MLV_free_image(non);
}

void home() {
/* Ceci est la page d'accueil où le joueur a le choix entre lancer une partie ou regarder la page d'aide */
    /* On charge les images */
    MLV_Image *fond = MLV_load_image("../data/fond.png");
    MLV_Image *retour = MLV_load_image("../data/retour.png");
    MLV_Image *parchment = MLV_load_image("../data/parchment.png");
    MLV_Image *fondDebut = MLV_load_image("../data/fondDebut.png");
    MLV_Image *fondPartie = MLV_load_image("../data/fondPartie.png");
    int x, y, buttonClique = 1;

    MLV_resize_image_with_proportions( fondDebut, 800, 750 );
    MLV_draw_image( fondDebut, 0, 0 );
    MLV_actualise_window();
    /* On attend que le joueur clique sur PLAY ou sur HELP */
    do {
        MLV_wait_mouse( &x, &y );
        if ( x >= 500 && x <= 690 && y >= 445 && y <= 515 ) { /* Cliquer sur PLAY */
            play( fondPartie );
        }
        else if (x >= 500 && x <= 690 && y >= 552 && y <= 625 ) /* Cliquer sur HELP */
            help( fond, parchment, retour );
    } while( buttonClique );
    /* On libère les images */
    MLV_free_image(fond);
    MLV_free_image(retour);
    MLV_free_image(parchment);
    MLV_free_image(fondDebut);
    MLV_free_image(fondPartie);
}

void help( MLV_Image* fond, MLV_Image* parchment, MLV_Image* retour ) {
    int x, y, buttonClique = 1;
    MLV_clear_window( MLV_COLOR_WHITE );
    MLV_draw_image( fond, 0, 0 );
    MLV_draw_image( parchment, 50, 120 );
    MLV_draw_image( retour, 600, 20 );
    MLV_actualise_window();
    /* On attend que le joueur clique sur le bouton RETOUR */
    do {
        MLV_wait_mouse( &x, &y );
        if ( x >= 600 && x <= 785 && y >= 30 && y <= 100 ) { /* Cliquer sur RETOUR */
            buttonClique = 0;
            home(); /* Cliquer sur RETOUR fait revenir le joueur à la page d'accueil */
            return ;
        }
    } while( buttonClique );
}

char* boiteDeSaisie( MLV_Image* fond, char* message ) {
/* Crée une boîte de saisie, récupère du texte et le retourne */
    char* texte;
    int taille;
    MLV_Input_box *input_box;
    MLV_Event event;
    /* Créé la boîte de saisie. */
    input_box = MLV_create_input_box(
                480, 170, 200, 30, 
                MLV_COLOR_BLACK, MLV_COLOR_BLACK, MLV_COLOR_WHITE, message
    );
    texte = (char*) malloc( 1*sizeof(char) ); *texte = '\0';

    /*MLV_draw_image( fond, 0, 0 );
    MLV_resize_image_with_proportions( fond, 800, 750 );*/
    MLV_draw_all_input_boxes();     /* Dessine toutes les boîtes de saisies */
    MLV_actualise_window();
    /* On attend que l'utilisateur saisisse du texte */
    do {
        event = MLV_get_event( NULL, NULL, NULL, &texte, &input_box, NULL, NULL, NULL, NULL );
      /*  MLV_draw_image(fond, 0, 0);
        MLV_resize_image_with_proportions(fond,800,750);*/
        MLV_draw_all_input_boxes();     /* Dessine toutes les boîtes de saisies */
        MLV_actualise_window();
    } while( !strcmp( texte, "" ) );

    /* Ferme la boîte de saisie. */
    MLV_free_input_box( input_box );
    taille = strlen(texte);
    texte[taille] = '\n';
    texte[taille+1] = '\0';
    /* On retourne le texte saisit dans la boîte de saisie */
    return texte;
}

void genieDevinePersonnage ( ListePersonnages listP, MLV_Image* fondPartie, ListePersonnages *meilleursPersonnages ) {
/* Une fois que l'utilisateur a répondu aux questions, le génie lui propose sa meilleur réponse */
	*meilleursPersonnages = triPersonnages( listP );
	afficherTexteFenetre( "Je pense à ...", fondPartie );
	MLV_wait_seconds(2);
	afficherTexteFenetre( (*meilleursPersonnages)->nameCharacter, fondPartie );
	MLV_wait_seconds(2);
}
